import React, { Component } from 'react';
import './App.css';
import Table from './Table';
import TextField from './TextField';
import superagent from 'superagent';



class App extends Component {
	
	constructor(props) {
		super(props);
		

		
		this.state = {
			users: [
				{
				'name': 'Charlie',
				'id': '0'
			},
			{
				'name': 'Mac',
				'id': '1'
			},
			{
				'name': 'Dee',
				'id': '2'
			},
			{
				'name': 'Dennis',
				'id': '3'
			}
			]       
		 };
		 
		 this.state.username = "";
		 this.state.password = "";
		 this.state.loginDetails = false;
		 
		 this.state.editIndex = null;
		 this.state.newname = "";
	}
	
	
	
	
	setLoginUserName = (username) =>{		
		this.setState({username : username.target.value});
	}
	
	setLoginPassword = (password) =>{		
		this.setState({password : password.target.value});
	}
	
	
	
	
	
	
	//sends request to server to remove user with given ID
	removeUser = (index, callback) => {
		
		var Base64 = require('base-64');
		
		var url = 'http://localhost:8080/secured/deleteUser/';
		
		if(!this.state.loginDetails){
			alert("No user is logged in.");
			return;
		}
	
		superagent
		   .delete(url)
		   .send({id: index})
		   .set('Accept', 'application/json')
		   .set('Authorization', "Basic " + Base64.encode(this.state.username+":"+this.state.password))
		   .then(res => {
			  //alert('Success ' + JSON.stringify(res.body));
			  
			  callback(index);
			}).catch((error) => {
				alert("Error - Could not remove user.\n");
			});	;

	}	
	
	//removes user with given id from state, after receiving a confirmation from server
	removeUserById = index => {
		const { users } = this.state;
		this.setState({
			users: users.filter((user, i) => { 
				return user.id !== index;
			})
		});
	}
	
	
	
	
	
	
	
	enableEdit = index =>{		
		this.setState({editIndex: index});		
	}
	
	setNewName = (e) =>{		
		this.setState({newname: e.target.value});
	}
	
	updateUser = () =>{
		
		var url = 'http://localhost:8080/updateUser';
		
		superagent
			.put(url)
			.send({id: this.state.editIndex, name: this.state.newname})
			.then(response => {
				if(response.body==null){
					alert("Error - Could not edit user");					
				}else{
					var arr = this.state.users;					
					var index = arr.findIndex((arr => arr.id === response.body.id));
					arr[index] = response.body;
					
					this.setState({ users: arr })
					
					this.setState({editIndex: null});
					this.setState({newname: ""});


					
				}
			}).catch((error) => {
			  alert("Error - Could not edit user.\n"+error.response.text);
			});
	}
	
	
	addUser = (name, username, password) =>{
		var url = 'http://localhost:8080/addUser';
		
		superagent
			.post(url)
			.send({name: name, username: username, password: password})
			.then(response => {
				if(response.body==null){
					alert("Error - Could not add new user.");					
				}else{
					var joined = this.state.users.concat(response.body);
					this.setState({ users: joined })					
				}
			}).catch((error) => {
			  alert("Error - Could not add new user.\n"+error.response.text);
			});		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

	//loads initial data from server
	componentDidMount() {		
		
		var url = 'http://localhost:8080/list';
		
		superagent
			.get(url)
			.then(response => {
				  //alert(JSON.stringify(users, null, 4))
			  var users = response.body;
			  this.setState({ users });
			});
			
	  }
	  
	  
	  
	
	
	
	
	render() {
		const { users } = this.state;
		
			
		return (
			<div className="App">
				<header className="App-header">
				
					<div>	{!this.state.loginDetails?(
								<div id="LoginForm">
										<h1>Login</h1>
										<input type="text" placeholder="Userame" onChange={(evt) => this.setLoginUserName(evt)}/>
										<input type="password" placeholder="Password" onChange={(evt) => this.setLoginPassword(evt)}/>
										<button onClick={() =>  this.setState({loginDetails : true})}>Login</button>	
								</div>
							):(
								<div id="LoginForm">
									<h1>Logout</h1>
									<button onClick={() => {
											this.setState({loginDetails : false})
											this.setState({userame : ""});
											this.setState({password : ""});
										} 
										
										}>Logout</button>
								</div>
							)
						}
					
						
						
					</div>
					<div className="container">
					
						 <Table
							userData={users}
							removeUser={this.removeUser}
							onRowDeleted={this.removeUserById}
							enableEdit={this.enableEdit}
						/>
						
							
							{/*shows edit from only after pressing edit button*/}
							{this.state.editIndex !== null &&
								<div>
									<h3>Edit ID: {this.state.editIndex}</h3>
									<input type="text" placeholder="New name" onChange={(evt) => this.setNewName(evt)}/>
									<br/>
									<button onClick={() =>  this.updateUser()}>Save</button>	
									<button onClick={() =>  this.setState({editIndex : null})}>Cancel</button>	
								</div>					
							}		
						
						
					</div>
					
					<TextField addUser={this.addUser} />	
					
						
					
						
					
				</header>
			</div>
		);
	}
}












export default App;
