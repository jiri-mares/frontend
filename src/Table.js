import React, {Component} from 'react';

import PropTypes from 'prop-types';



class Table extends Component {
    render() {
        const { userData, removeUser, onRowDeleted, enableEdit } = this.props;

        return (
            <table>
                <TableHeader />
                <TableBody 
                    userData={userData} 
                    removeUser={removeUser} 
					onRowDeleted={onRowDeleted}
					enableEdit={enableEdit}
                />
            </table>
        );
    }
}

Table.propTypes = {
	userData: PropTypes.array
};






const TableHeader = () => { 
    return (
        <thead>
            <TableRow>
                <TableCol>Name</TableCol>
                <TableCol>ID</TableCol>
            </TableRow>
        </thead>
    );
}





const TableBody = props => { 
    const rows = props.userData.map((row, index) => {
        return (
            <TableRow key={index}>
				<TableCol>{row.name}</TableCol>
				<TableCol>{row.id}</TableCol>
				<TableCol>
				<button onClick={() => props.removeUser(row.id, props.onRowDeleted)}>Delete</button>
				<button onClick={() => props.enableEdit(row.id)}>Edit</button>
				
				</TableCol>
			</TableRow>
			
        );
    });

    return <tbody>{rows}</tbody>;
}



const TableRow = props => {	
	return (
		<tr>{props.children}</tr>	
	)	
}


const TableCol = props => {
	return (
		<td>{props.children}</td>	
	)	
}





export default Table;
