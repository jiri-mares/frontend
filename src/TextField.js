import React, {Component} from 'react';







class TextField extends Component {	
	
	constructor(props) {
		super(props);
		
		this.state = {
			name: "",
			username: "",
			password: "",			
		}
	};
	

	ChangeName = (e) => {
		this.setState({ name: e.target.value });
	}
	
	
	
	ChangeUsername = (e) => {
		this.setState({ username: e.target.value });
	}
	
	ChangePassword = (e) => {
		this.setState({ password: e.target.value });
	}

	
	
	
	AddButtonClick = () => {
		//alert(this.state.input);
		this.props.addUser(this.state.name, this.state.username, this.state.password );
	}
	
	
    render() {
		
        return (
		
		<div>
			<h1>Add user</h1>
            <input type="text" placeholder="Name" onChange={(evt) => this.ChangeName(evt)}/>
			<br/>
            <input type="text" placeholder="Userame" onChange={(evt) => this.ChangeUsername(evt)}/>
			<br/>
            <input type="password" placeholder="Password" onChange={(evt) => this.ChangePassword(evt)}/>
			<br/>
			<button onClick={() => this.AddButtonClick() }>Add</button>
		</div>
        );
    }
}


export default TextField;
 
